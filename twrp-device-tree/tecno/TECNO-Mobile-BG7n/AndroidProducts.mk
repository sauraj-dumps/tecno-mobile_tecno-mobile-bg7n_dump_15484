#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_TECNO-Mobile-BG7n.mk

COMMON_LUNCH_CHOICES := \
    omni_TECNO-Mobile-BG7n-user \
    omni_TECNO-Mobile-BG7n-userdebug \
    omni_TECNO-Mobile-BG7n-eng
