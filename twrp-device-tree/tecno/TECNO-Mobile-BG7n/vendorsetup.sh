#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_TECNO-Mobile-BG7n-user
add_lunch_combo omni_TECNO-Mobile-BG7n-userdebug
add_lunch_combo omni_TECNO-Mobile-BG7n-eng
