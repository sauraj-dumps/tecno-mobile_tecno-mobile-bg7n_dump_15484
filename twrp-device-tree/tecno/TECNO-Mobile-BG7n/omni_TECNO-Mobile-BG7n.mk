#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from TECNO-Mobile-BG7n device
$(call inherit-product, device/tecno/TECNO-Mobile-BG7n/device.mk)

PRODUCT_DEVICE := TECNO-Mobile-BG7n
PRODUCT_NAME := omni_TECNO-Mobile-BG7n
PRODUCT_BRAND := TECNO-Mobile
PRODUCT_MODEL := TECNO Mobile BG7n
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-tecno

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="vnd_bg7n_xe674s-user 12 SP1A.210812.016 493850 release-keys"

BUILD_FINGERPRINT := TECNO-Mobile/BG7n-EU/TECNO-Mobile-BG7n:12/SP1A.210812.016/231130V794:user/release-keys
